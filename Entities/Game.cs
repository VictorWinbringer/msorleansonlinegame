﻿using System.Collections.Generic;
using System.Linq;
using Orleans.Concurrency;

namespace Entities
{
    [Immutable]
    public sealed class Game
    {
        public long Id { get; set; }
        public List<Player> Players { get; set; } = new List<Player>();
        public List<Bullet> Bullets { get; set; } = new List<Bullet>();
        public Frame Frame { get; set; } = new Frame();

        public Game Clone() => new Game()
        {
            Id = Id,
            Players = new List<Player>(Players.Select(p => p.Clone())),
            Bullets = new List<Bullet>(Bullets.Select(b => b.Clone())),
            Frame = Frame.Clone()
        };
    }
}