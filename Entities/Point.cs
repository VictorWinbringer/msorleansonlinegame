﻿using System;
using Orleans.Concurrency;

namespace Entities
{
    [Immutable]
    public sealed class Point : IEquatable<Point>
    {
        public byte X { get; set; }
        public byte Y { get; set; }
        public void Move(Direction direction)
        {
            var _ = direction switch
            {
                Direction.Bottom when Y < 255 => Y++,
                Direction.Top when Y > 0 => Y--,
                Direction.Right when X < 255 => X++,
                Direction.Left when X > 0 => X--,
                _ => 0
            };
        }

        public bool Equals(Point other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is Point other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        public Point Clone() => new Point() { X = X, Y = Y };
    }
}
