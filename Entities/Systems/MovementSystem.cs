﻿using Entities.Components;

namespace Entities.Systems
{
    public static class MovementSystem
    {
        public static void Move(this IMovableComponent component, double deltaTime)
        {
            component.TotalTime += deltaTime;
            if (component.TotalTime < component.MoveTime)
                return;
            component.TotalTime -= component.MoveTime;
            component.Point.Move(component.Direction);
        }

        public static void MoveBack(this IMovableComponent component) =>
            component.Point.Move(component.Direction.Reverse());
    }
}