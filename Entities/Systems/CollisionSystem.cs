﻿using Entities.Components;

namespace Entities.Systems
{
    public static class CollisionSystem
    {
        public static bool Collide(this ICollisionComponent lhs, ICollisionComponent rhs)
        {
            if (lhs?.Points == null)
                return false;
            if (rhs?.Points == null)
                return false;
            foreach (var lhsPoint in lhs.Points)
            {
                if (lhsPoint == null)
                    continue;
                foreach (var rhsPoint in rhs.Points)
                {
                    if (rhsPoint == null)
                        continue;
                    if (lhsPoint.Equals(rhsPoint))
                        return true;
                }
            }
            return false;
        }
    }
}