﻿using System;

namespace Entities
{
    public interface IGameEntity
    {
        Guid Id { get; set; }
        bool IsAlive { get; set; }
    }
}