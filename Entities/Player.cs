﻿using System;
using System.Collections.Generic;
using Entities.Components;
using Orleans.Concurrency;

namespace Entities
{
    [Immutable]
    public sealed class Player : IMovableComponent, ICollisionComponent, IGameEntity
    {
        public Guid Id { get; set; }
        public long? GameId { get; set; }
        public Point Point { get; set; } = new Point() { X = 5, Y = 5 };
        public double TotalTime { get; set; }
        public double MoveTime { get; set; } = 50;
        public double ShotColldown { get; set; } = 500;
        public double TimeAfterLastShot { get; set; }
        public Direction Direction { get; set; } = Direction.Right;
        public bool IsAlive { get; set; }
        public List<Point> Points => new List<Point> { Point };
        public bool HasColldown => TimeAfterLastShot < ShotColldown;

        public Bullet Shot()
        {
            if (HasColldown)
                return null;
            TimeAfterLastShot = 0;
            var point = Point.Clone();
            point.Move(Direction);
            return new Bullet()
            {
                Id = Guid.NewGuid(),
                Direction = Direction,
                GameId = GameId,
                IsAlive = true,
                Point = point,
                PlayerId = Id
            };
        }

        public Player Clone() => new Player()
        {
            Direction = Direction,
            GameId = GameId,
            Id = Id,
            IsAlive = IsAlive,
            Point = Point.Clone()
        };
    }
}