﻿using System;
using System.Collections.Generic;
using Entities.Components;
using Orleans.Concurrency;

namespace Entities
{
    [Immutable]
    public sealed class Bullet : IMovableComponent, ICollisionComponent, IGameEntity
    {
        public Guid Id { get; set; }
        public Guid PlayerId { get; set; }
        public Point Point { get; set; } = new Point();
        public double TotalTime { get; set; }
        public double MoveTime { get; set; } = 20;
        public Direction Direction { get; set; }
        public bool IsAlive { get; set; }
        public long? GameId { get; set; }
        public List<Point> Points => new List<Point>() { Point };

        public Bullet Clone() => new Bullet()
        {
            Direction = Direction,
            GameId = GameId,
            Id = Id,
            IsAlive = IsAlive,
            Point = Point.Clone(),
            PlayerId = PlayerId
        };

        public bool IsInFrame(Frame frame) => Point.X <= frame.Width && Point.Y <= frame.Height;
    }
}