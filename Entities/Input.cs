﻿using System;
using System.Collections.Generic;
using Orleans.Concurrency;

namespace Entities
{
    [Immutable]
    public sealed class Input
    {
        public long Id { get; set; }
        public Guid PlayerId { get; set; }
        public List<Command> Commands { get; set; } = new List<Command>();
        public List<Direction> Directions { get; set; } = new List<Direction>();

        public Input Clone() => new Input()
        {
            Commands = new List<Command>(Commands),
            Directions = new List<Direction>(Directions),
            PlayerId = PlayerId
        };

    }
}