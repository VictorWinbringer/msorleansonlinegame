﻿using System.Collections.Generic;

namespace Entities.Components
{
    public interface ICollisionComponent
    {
        List<Point> Points { get; }
    }
}