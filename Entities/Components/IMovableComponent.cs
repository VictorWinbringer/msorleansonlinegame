﻿namespace Entities.Components
{
    public interface IMovableComponent
    {
        Point Point { get; set; }
        double TotalTime { get; set; }
        double MoveTime { get; set; }
        Direction Direction { get; set; }
    }
}