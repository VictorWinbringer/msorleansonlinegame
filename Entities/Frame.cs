﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.Components;
using Orleans.Concurrency;

namespace Entities
{
    [Immutable]
    public sealed class Frame : ICollisionComponent
    {
        public Frame(int width, int height)
        {
            Width = width;
            Height = height;
            var points = new List<Point>();
            for (byte i = 0; i < width; i++)
            {
                for (byte j = 0; j < height; j++)
                {
                    if (IsBorder(i, width - 1) ||
                        IsBorder(j, height - 1) ||
                        IsDiagonal(width / (double)height, i, j, width, height))
                        points.Add(new Point() { X = i, Y = j });
                }
            }

            Points = points;
        }

        public Frame()
        {
            Points = new List<Point>();
        }

        public int Width { get; set; }
        public int Height { get; set; }
        public List<Point> Points { get; set; }
        public long GameId { get; set; }

        public Frame Clone() => new Frame()
        {
            Points = new List<Point>(Points.Select(p => p.Clone())),
            GameId = GameId,
            Width = Width,
            Height = Height
        };

        private bool IsBorder(int value, int max) => value == max || value == 0;

        private bool IsDiagonal(double a, int x, int y, int maxX, int maxY) =>
            x == (int)Math.Round(y * a) && InDiv(x, maxX / 5) && InDiv(y, maxY / 5);

        private bool InDiv(int i, int div) => (i > div && i <= div * 2) || (i > div * 3 && i <= div * 4);
    }
}