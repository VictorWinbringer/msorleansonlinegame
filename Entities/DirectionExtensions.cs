﻿using System;

namespace Entities
{
    public static class DirectionExtensions
    {
        public static Direction Reverse(this Direction direction) => direction switch
        {
            Direction.Left => Direction.Right,
            Direction.Right => Direction.Left,
            Direction.Bottom => Direction.Top,
            Direction.Top => Direction.Bottom,
            _ => throw new ApplicationException("Неизвестное направление") { Data = { ["direction"] = direction } }
        };
    }
}