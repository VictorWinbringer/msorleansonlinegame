﻿using System.Threading.Tasks;
using Entities;
using Orleans;

namespace Grains.Interfaces
{
    public interface IGame : IGrainWithIntegerKey
    {
        Task Update(Player player);
        Task Update(Bullet bullet);
    }
}