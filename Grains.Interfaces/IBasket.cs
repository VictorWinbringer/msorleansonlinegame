﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orleans;

namespace Grains.Interfaces
{
    public interface IBasket : IGrainWithGuidKey
    {
        Task Add(string item);
        Task Remove(string item);
        Task<List<string>> GetItems();
    }
}