﻿using System.Threading.Tasks;
using Entities;
using Orleans;

namespace Grains.Interfaces
{
    public interface IPlayer : IGrainWithGuidKey
    {
        Task Handle(Input input);
        Task Die();
        Task Update(double deltaTime, Game game);
        Task JoinGame(long gameId);
    }
}