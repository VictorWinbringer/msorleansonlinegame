﻿using System.Threading.Tasks;
using Entities;
using Orleans;

namespace Grains.Interfaces
{
    public interface IBullet : IGrainWithGuidKey
    {
        Task Update(Bullet dto);
        Task Update(double deltaTime, Game game);
    }
}