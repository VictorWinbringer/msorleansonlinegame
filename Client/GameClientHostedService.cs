﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;

namespace Client
{
    public class GameClientHostedService : IHostedService
    {
        private bool _stop;
        private readonly Guid _playerId;
        private readonly long _gameId;

        private IDisposable _hub;
        private HubConnection _connection;
        private DateTime _lastInputTime;
        private Thread _inputTask;
        private Thread _displayTask;
        private Thread _keysTask;
        private readonly ConcurrentQueue<Game> _buffer;
        private readonly ConcurrentQueue<ConsoleKey> _keys;
        public GameClientHostedService()
        {
            //Тут по идее надо бы сохранить идентификатор игрока в каком нибудь хранилище чтобы 
            // при переподключении он не терял свое состояние или сделать авторизацию и по токену SSO определять игрока.
            //Для учебного сойдет и рандомный Guid
            _playerId = Guid.NewGuid();
            _gameId = 1L;
            _buffer = new ConcurrentQueue<Game>();
            _lastInputTime = DateTime.Now;
            _keys = new ConcurrentQueue<ConsoleKey>();
        }

        private async Task Draw(Game game)
        {
            foreach (var player in game.Players)
            {
                if (!player.IsAlive)
                    continue;
                Console.SetCursorPosition(player.Point.X, player.Point.Y);
                if (player.Id == _playerId)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    await Console.Out.WriteAsync('#');
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    await Console.Out.WriteAsync('#');
                }
            }
            Console.ForegroundColor = ConsoleColor.Red;
            foreach (var bullet in game.Bullets)
            {
                if (!bullet.IsAlive)
                    continue;
                Console.SetCursorPosition(bullet.Point.X, bullet.Point.Y);
                await Console.Out.WriteAsync('$');
            }
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var point in game.Frame.Points)
            {
                Console.SetCursorPosition(point.X, point.Y);
                await Console.Out.WriteAsync('*');
            }
        }

        private static async Task Connect(HubConnection connection)
        {
            while (connection.State == HubConnectionState.Disconnected)
            {
                try
                {
                    await connection.StartAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _stop = false;
            Console.Clear();
            Console.WriteLine("LOADING");
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Для передвижения используйте клавиши W A S D или стрелочки на клавиатуре");
            Console.WriteLine("Для стрельбы нажмите пробел");
            Console.WriteLine("Выстрел происходит в тут сторону куда вы движитесь");
            Console.WriteLine("Для начала игры нажмите Enter");
            Console.WriteLine(_playerId);
            Console.ReadLine();
            Console.SetWindowSize(150, 75);
            Console.SetBufferSize(150, 75);
            Console.CursorVisible = false;
            Console.CursorSize = 2;

            var connection = new HubConnectionBuilder()
                .WithAutomaticReconnect()
                .WithUrl(new Uri("http://localhost:5000/game"))
                .Build();
            _connection = connection;
            await Connect(connection);
            _hub = connection.On<Game>("gameUpdated", state =>
            {
                if (_playerId == Guid.Empty)
                    return;
                _buffer.Enqueue(state);
            });
            await connection.SendAsync("JoinGame", _gameId, _playerId, cancellationToken);
            Console.Clear();
            _keysTask = new Thread(() =>
            {
                while (!_stop)
                {
                    var key = Console.ReadKey(true);
                    _keys.Enqueue(key.Key);
                }
            });
            _inputTask = new Thread(async () =>
            {
                long id = 0;
                while (!_stop)
                {
                    if ((DateTime.Now - _lastInputTime).TotalMilliseconds < 50)
                        continue;
                    _lastInputTime = DateTime.Now;
                    var commands = new List<Command>();
                    var directions = new List<Direction>();
                    while (_keys.TryDequeue(out ConsoleKey key))
                    {
                        var command = ToCommand(key);
                        var direction = ToDirection(key);
                        if (direction.HasValue)
                            directions.Add(direction.Value);
                        if (command.HasValue)
                            commands.Add(command.Value);
                    }
                    try
                    {
                        await connection.SendAsync("GameInput", new Input()
                        {
                            Id = ++id,
                            Directions = directions,
                            Commands = commands,
                            PlayerId = _playerId
                        }, _gameId, cancellationToken);
                    }
                    catch (Exception e)
                    {
                        Console.SetCursorPosition(50, 50);
                        Console.Clear();
                        Console.WriteLine($"Exception while trying to run client: {e.Message}");
                        Console.WriteLine("Make sure the silo the client is trying to connect to is running.");
                        Console.WriteLine("Press Enter to continue.");
                        await Connect(connection);
                    }
                }
            });
            _displayTask = new Thread(async () =>
            {
                while (!_stop)
                {
                    if (!_buffer.TryDequeue(out Game state))
                    {
                        await Task.Yield();
                        continue;
                    }
                    Console.CursorVisible = false;
                    Console.Clear();
                    await Draw(state);
                    await Console.Out.FlushAsync();
                }
            });
            _keysTask.Start();
            _inputTask.Start();
            _displayTask.Start();
        }

        public Command? ToCommand(ConsoleKey key) => key switch
        {
            ConsoleKey.Spacebar => Command.Shoot,
            _ => null
        };

        public Direction? ToDirection(ConsoleKey key) => key switch
        {
            ConsoleKey.LeftArrow => Direction.Left,
            ConsoleKey.A => Direction.Left,
            ConsoleKey.RightArrow => Direction.Right,
            ConsoleKey.D => Direction.Right,
            ConsoleKey.UpArrow => Direction.Top,
            ConsoleKey.W => Direction.Top,
            ConsoleKey.DownArrow => Direction.Bottom,
            ConsoleKey.S => Direction.Bottom,
            _ => null
        };

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _stop = true;
            _hub?.Dispose();
            _hub = null;
            if (_connection != null)
                await _connection.DisposeAsync();
            _connection = null;
        }
    }
}