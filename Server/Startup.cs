using System;
using System.Collections.Generic;
using Akka.Actor;
using Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Orleans.Runtime;
using Server.AkkaActors;
using Server.Hubs;

namespace Server
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
            services.AddControllers();
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" }); });
            services.AddSingleton(ActorSystem.Create("game"));
            var games = new Dictionary<long, IActorRef>();
            services.AddSingletonNamedService<Func<long, IActorRef>>(
                "game", (sp, name) => gameId =>
            {
                lock (games)
                {
                    if (!games.TryGetValue(gameId, out IActorRef gameActor))
                    {
                        var frame = new Frame(GameActor.WIDTH, GameActor.HEIGHT) { GameId = gameId };
                        var gameEntity = new Game()
                        {
                            Id = gameId,
                            Frame = frame
                        };
                        var props = Props.Create(() => new GameActor(gameEntity, sp));
                        var actorSystem = sp.GetRequiredService<ActorSystem>();
                        gameActor = actorSystem.ActorOf(props, gameId.ToString());
                        games[gameId] = gameActor;
                    }
                    return gameActor;
                }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                c.RoutePrefix = string.Empty;
            });
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<GameHub>("/game");
            });
        }
    }
}
