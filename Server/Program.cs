using System.Net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Server.Grains;

namespace Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }).UseOrleans(b =>
                {
                    b.UseLocalhostClustering()
                   .Configure<ClusterOptions>(options =>
                   {
                       options.ClusterId = "dev";
                       options.ServiceId = "OrleansBasics";
                   })
                   .Configure<EndpointOptions>(
                       options => options.AdvertisedIPAddress = IPAddress.Loopback
                   )
                   .AddMemoryGrainStorage("gameState")
                   .AddMemoryGrainStorage("shopState")
                   .ConfigureApplicationParts(
                       parts => parts
                           .AddApplicationPart(typeof(GameGrain).Assembly)
                           .WithReferences()
                   );
                });
    }
}
