﻿using System;
using Entities;

namespace Server.AkkaActors.Messages
{
    public class InitPlayerMessage
    {
        public Player Player { get; }
        public Frame Frame { get; }

        public InitPlayerMessage(Player player, Frame frame)
        {
            Player = player ?? throw new ApplicationException("Укажите игрока!");
            Frame = frame ?? throw new ApplicationException("Укажите фрейм");
        }
    }
}