﻿using System;
using System.Collections.Generic;
using Entities;

namespace Server.AkkaActors.Messages
{
    public sealed class UpdateMessage
    {
        public double DeltaTime { get; }
        public List<Player> Players { get; }

        public UpdateMessage(double deltaTime, List<Player> players)
        {
            if(deltaTime < 0)
                throw new ApplicationException("Дельта времени меньше нуля!");
            DeltaTime = deltaTime;
            Players = players ?? throw new ApplicationException("Укажите игроков!");
        }
    }
}