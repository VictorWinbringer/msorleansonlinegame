﻿using System;
using Entities;

namespace Server.AkkaActors.Messages
{
    public sealed class InitBulletMessage
    {
        public Bullet Bullet { get; }
        public Frame Frame { get; }

        public InitBulletMessage(Bullet bullet, Frame frame)
        {
            Bullet = bullet ?? throw new ApplicationException("Укажите пулю");
            Frame = frame ?? throw new ApplicationException("Укажите фрейм");
        }
    }
}