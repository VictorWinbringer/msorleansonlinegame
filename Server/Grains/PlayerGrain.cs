﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Entities;
using Entities.Systems;
using Grains.Interfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;

namespace Server.Grains
{
    public class PlayerGrain : Grain, IPlayer
    {
        private readonly ILogger<PlayerGrain> _logger;
        private readonly IPersistentState<Player> _store;
        private IDisposable _timer;
        private readonly Queue<Input> _inputs;

        public PlayerGrain(
            ILogger<PlayerGrain> logger,
            [PersistentState("player", "gameState")] IPersistentState<Player> store
        )
        {
            _logger = logger;
            _store = store;
            _inputs = new Queue<Input>();
        }

        public override Task OnActivateAsync()
        {
            _logger.LogInformation("ACTIVATED");
            _store.State.Id = this.GetPrimaryKey();
            return base.OnActivateAsync();
        }

        public override async Task OnDeactivateAsync()
        {
            _logger.LogInformation("ACTIVATED");
            _timer?.Dispose();
            _timer = null;
            await _store.WriteStateAsync();
            await base.OnDeactivateAsync();
        }

        public Task Handle(Input input)
        {
            if (_store.State.GameId.HasValue && _store.State.IsAlive)
                _inputs.Enqueue(input);
            return Task.CompletedTask;
        }

        public async Task Update(double deltaTime, Game game)
        {
            if (!_store.State.GameId.HasValue)
            {
                await GrainFactory.GetGrain<IGame>(game.Id).Update(_store.State.Clone());
                await _store.ClearStateAsync();
                DeactivateOnIdle();
                return;
            }
            while (_inputs.Count > 0)
            {
                var input = _inputs.Dequeue();
                foreach (var direction in input.Directions)
                {
                    _store.State.Direction = direction;
                }

                _store.State.TimeAfterLastShot += deltaTime;
                if (!_store.State.HasColldown)
                    foreach (var command in input.Commands)
                    {
                        if (command == Command.Shoot && _store.State.GameId.HasValue)
                        {
                            var dto = _store.State.Shot();
                            var bullet = GrainFactory.GetGrain<IBullet>(dto.Id);
                            await bullet.Update(dto);
                            await GrainFactory.GetGrain<IGame>(_store.State.GameId.Value).Update(dto);
                            break;
                        }
                    }
            }
            Move(deltaTime, game);
            await GrainFactory.GetGrain<IGame>(_store.State.GameId.Value)
                 .Update(_store.State.Clone());
            if (!_store.State.IsAlive)
            {
                await _store.ClearStateAsync();
                DeactivateOnIdle();
            }
        }

        private void Move(double deltaTime, Game game)
        {
            _store.State.Move(deltaTime);
            if (game.Frame.Collide(_store.State))
                _store.State.MoveBack();
        }

        public async Task JoinGame(long gameId)
        {
            var game = GrainFactory.GetGrain<IGame>(gameId);
            _store.State.GameId = gameId;
            _store.State.IsAlive = true;
            await game.Update(_store.State);
        }

        public async Task Die()
        {
            _store.State.IsAlive = false;
            if (_store.State.GameId.HasValue)
                await GrainFactory.GetGrain<IGame>(_store.State.GameId.Value).Update(_store.State.Clone());
            await _store.ClearStateAsync();
            DeactivateOnIdle();
        }
    }
}