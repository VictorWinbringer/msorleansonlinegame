﻿using System.Linq;
using System.Threading.Tasks;
using Entities;
using Entities.Systems;
using Grains.Interfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;

namespace Server.Grains
{
    public class BulletGrain : Grain, IBullet
    {
        private readonly ILogger<BulletGrain> _logger;
        private readonly IPersistentState<Bullet> _store;
        public BulletGrain(
            ILogger<BulletGrain> logger,
            [PersistentState("bullet", "gameState")] IPersistentState<Bullet> store
        )
        {
            _logger = logger;
            _store = store;
        }

        public Task Update(Bullet dto)
        {
            dto.Id = this.GetPrimaryKey();
            _store.State = dto;
            return Task.CompletedTask;
        }

        public override Task OnActivateAsync()
        {
            _logger.LogInformation("ACTIVATED");
            _store.State.Id = this.GetPrimaryKey();
            return base.OnActivateAsync();
        }

        public async Task Update(double deltaTime, Game game)
        {
            if (!_store.State.GameId.HasValue)
            {
                await GrainFactory.GetGrain<IGame>(game.Id).Update(_store.State.Clone());
                await _store.ClearStateAsync();
                DeactivateOnIdle();
            }
            Move(deltaTime);
            if (game.Frame.Collide(_store.State))
                _store.State.IsAlive = false;
            if (!_store.State.IsInFrame(game.Frame))
                _store.State.IsAlive = false;
            foreach (var player in game.Players.Where(p => p.IsAlive))
            {
                if (player.Id == _store.State.PlayerId)
                    continue;
                if (player.Collide(_store.State))
                {
                    _store.State.IsAlive = false;
                    await GrainFactory.GetGrain<IPlayer>(player.Id).Die();
                    break;
                }
            }
            await GrainFactory
                .GetGrain<IGame>(_store.State.GameId.Value)
                .Update(_store.State.Clone());
            if (!_store.State.IsAlive)
            {
                await _store.ClearStateAsync();
                DeactivateOnIdle();
            }
        }

        private void Move(double deltaTime)
        {
            _store.State.Move(deltaTime);
        }
    }
}