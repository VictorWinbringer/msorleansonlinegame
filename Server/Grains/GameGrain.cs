﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Grains.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;
using Server.Hubs;

namespace Server.Grains
{
    public class GameGrain : Grain, IGame
    {
        private const byte WIDTH = 100;
        private const byte HEIGHT = 50;
        private readonly ILogger<GameGrain> _logger;
        private readonly IPersistentState<Game> _store;
        private readonly IHubContext<GameHub> _hub;
        private DateTime _updateTime;
        private IDisposable _updater;
        private double _totalTime;
        public GameGrain(
            ILogger<GameGrain> logger,
            [PersistentState("game", "gameState")] IPersistentState<Game> store,
            IHubContext<GameHub> hub
            )
        {
            _logger = logger;
            _store = store;
            _hub = hub;
            _updateTime = DateTime.Now;
        }

        public override async Task OnActivateAsync()
        {
            _store.State.Id = this.GetPrimaryKeyLong();
            _store.State.Frame = new Frame(WIDTH, HEIGHT) { GameId = _store.State.Id };
            _logger.LogWarning("ACTIVATED");
            _updater = RegisterTimer(Run, null, TimeSpan.FromMilliseconds(1), TimeSpan.FromMilliseconds(1));
            await base.OnActivateAsync();
        }

        public override async Task OnDeactivateAsync()
        {
            _logger.LogWarning("DEACTIVATED");
            _updater?.Dispose();
            _updater = null;
            await _store.WriteStateAsync();
            await base.OnDeactivateAsync();
        }

        public async Task Run(object obj)
        {
            var deltaTime = DateTime.Now - _updateTime;
            _updateTime = DateTime.Now;
            var delta = deltaTime.TotalMilliseconds;
            await Update(obj, delta);
            await Draw(obj, delta);
        }

        public async Task Update(object obj, double delta)
        {
            var gameId = this.GetPrimaryKeyLong();
            var state = _store.State.Clone();
            state.Bullets.RemoveAll(b => !b.IsAlive || b.GameId != gameId);
            state.Players.RemoveAll(p => !p.IsAlive || p.GameId != gameId);
            await Task.WhenAll(
                state
                    .Bullets
                    .Select(bullet => GrainFactory.GetGrain<IBullet>(bullet.Id).Update(delta, state))
                    .Concat(
                        state
                            .Players
                            .Select(player => GrainFactory.GetGrain<IPlayer>(player.Id).Update(delta, state))
                    )
                );
        }

        public async Task Draw(object obj, double delta)
        {
            _totalTime += delta;
            if (_totalTime < 50)
                return;
            _totalTime -= 50;
            var state = _store.State;
            try
            {
                await _hub.Clients.All.SendAsync("gameUpdated", state.Clone());
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error on send s");
            }
        }

        public Task Update(Player player)
        {
            _store.State.Players.RemoveAll(x => x.Id == player.Id);
            if (player.IsAlive)
                _store.State.Players.Add(player);
            return Task.CompletedTask;
        }

        public Task Update(Bullet bullet)
        {
            _store.State.Bullets.RemoveAll(x => x.Id == bullet.Id);
            if (bullet.IsAlive)
                _store.State.Bullets.Add(bullet);
            return Task.CompletedTask;
        }
    }
}
