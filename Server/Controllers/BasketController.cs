﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grains.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Orleans;

namespace Server.Controllers
{
    [ApiController]
    [Route("api/v1/basket")]
    public class BasketController : ControllerBase
    {
        private readonly IGrainFactory _grainFactory;

        public BasketController(IGrainFactory grainFactory)
        {
            _grainFactory = grainFactory;
        }

        [HttpPut("{id}/add-item")]
        public async Task Add(Guid id, string item)
        {
            await _grainFactory.GetGrain<IBasket>(id).Add(item);
        }

        [HttpPut("{id}/remove-item")]
        public async Task Remove(Guid id, string item)
        {
            await _grainFactory.GetGrain<IBasket>(id).Remove(item);
        }

        [HttpGet("{id}/get-items")]
        public async Task<List<string>> GetItems(Guid id)
        {
            return await _grainFactory.GetGrain<IBasket>(id).GetItems();
        }
    }
}
