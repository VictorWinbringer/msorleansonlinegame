﻿using System;
using System.Threading.Tasks;
using Akka.Actor;
using Entities;
using Microsoft.AspNetCore.SignalR;
using Orleans;
using Orleans.Runtime;
using Server.AkkaActors;

namespace Server.Hubs
{
    public class GameHub : Hub
    {
        private readonly ActorSystem _system;
        private readonly IServiceProvider _provider;
        private readonly IGrainFactory _client;

        public GameHub(
            IGrainFactory client,
            ActorSystem system,
            IServiceProvider provider
            )
        {
            _client = client;
            _system = system;
            _provider = provider;
        }

        public async Task JoinGame(long gameId, Guid playerId)
        {
            var gameFactory = _provider.GetRequiredServiceByName<Func<long, IActorRef>>("game");
            var game = gameFactory(gameId);
            var random = new Random();
            var player = new Player()
            {
                IsAlive = true,
                GameId = gameId,
                Id = playerId,
                Point = new Point()
                {
                    X = (byte)random.Next(1, GameActor.WIDTH - 1),
                    Y = (byte)random.Next(1, GameActor.HEIGHT - 1)
                }
            };
            game.Tell(player);

            //var player = _client.GetGrain<IPlayer>(playerId);
            //await player.JoinGame(gameId);
        }

        public async Task GameInput(Input input, long gameId)
        {
            _system.ActorSelection($"user/{gameId}/{input.PlayerId}").Tell(input);
            //var player = _client.GetGrain<IPlayer>(input.PlayerId);
            //await player.Handle(input);
        }
    }
}
